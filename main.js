import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { setupCounter } from './counter.js'

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello Vite!</h1>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
    <p class="read-the-docs">
      Click on the Vite logo to learn more
    </p>
  </div>
`

setupCounter(document.querySelector('#counter'))

/**
 * EXO ALGO http://cours.pise.info/algo/enonces1.htm#En1.1
 */

/**DEBUT ALGO 1.1**********************************************************************
************************************************************************
************************************************************************
let A = 1
let B = A + 3
A = 3

// donc A vaut 3 & B vaut 4

console.log(`A vaut 3 (vérification en console: A = ${A}`)
console.log(`B vaut 4 (vérification en console: B = ${B}`) 
// OK
// FIN
************************************************************************
************************************************************************
************************************************************************/

/**DEBUT ALGO 1.2*******************************************************
************************************************************************
************************************************************************

let A = 5
let B = 3
let C = A + B // C=5+3
A = 2         // A change de valeur & passe de 5 à 2
C = B - A     // C=3-2
// donc A = 2 & B = 3 

console.log(`A vaut 2 (vérification en console: A = ${A}`)
console.log(`B vaut 3 (vérification en console: B = ${B}`)
console.log(`C vaut ... (vérification en console: C = ${C}`)
// OK, FIN
************************************************************************
************************************************************************
************************************************************************/

/************************************************************************
************************************************************************
************************************************************************
// 1.3

let A = 5
let B = A + 4   // B = 9
A = A + 1       // A = 6
B = A - 4       // B = 6 - 4 // B = 2

console.log(`A vaut 6 (vérification en console: A = ${A}`)
console.log(`B vaut 2 (vérification en console: B = ${B}`)

// OK, FIN

/***********************************************************************
************************************************************************
************************************************************************
// 1.4

let A = 3
let B = 10
let C = A + B   // C = 13
B = A + B       // B = 13
A = C           // A = 13 (C = 13) 

console.log(`A vaut 13 (vérification en console: A = ${A}`)
console.log(`B vaut 13 (vérification en console: B = ${B}`)
console.log(`C vaut 13 (vérification en console: C = ${C}`)
// OK FIN ALGO
/***********************************************************************
************************************************************************
************************************************************************/

/***********************************************************************
************************************************************************
************************************************************************
// 1.5

let A = 5
let B = 2
A = B       // A = 2 
B = A       // B = 2

console.log(`A vaut 2 (vérification en console: A = ${A}`)
console.log(`B vaut 2 (vérification en console: B = ${B}`)
/***********************************************************************
************************************************************************
************************************************************************/

/***********************************************************************
************************************************************************
************************************************************************/
// 1.6
/**
 * Plus difficile, mais c’est un classique absolu, 
 * qu’il faut absolument maîtriser : 
 *    écrire un algorithme permettant d’échanger les 
 *    valeurs de deux variables A et B, 
 *    et ce quel que soit leur contenu préalable.
 *
let A = "foo"
let B = "bar"
let C = A   // il faut passer par 1 variable temporaire    
A = B       // A = B (soit "bar")
B = C       // B = A (soit "foo")
C = A + B   // les string s'enchaîne (C = "barfoo")

console.log(`A vaut "bar" (vérification en console: A = ${A}`)
console.log(`B vaut "foo" (vérification en console: B = ${B}`)
console.log(`C vaut "barfoo" (vérification en console: C = ${C}`)
/***********************************************************************
************************************************************************
************************************************************************/

/***********************************************************************
************************************************************************
************************************************************************
// 1.8

let A = "428"
let B = "12"
let C = A + B

console.log(`A vaut "428" (vérification en console: A = ${A}`)
console.log(`B vaut "12" (vérification en console: B = ${B}`)
console.log(`C vaut "42812" (vérification en console: C = ${C}`)
/***********************************************************************
************************************************************************
************************************************************************

/************************************************************************
************************************************************************
************************************************************************/
// 1.9

let A = "423"
let B = "12"
let C = A && B    // C = "423""12" (en JS, on ne peut pas concaténer avec ET)
// mais en JS, l'addition de 2 string fonctionne et les concatène
// ! ceci est propre au JS (à vérifier ds autres langages)

console.log(`A vaut "423" (vérification en console: A = ${A}`)
console.log(`B vaut "12" (vérification en console: B = ${B}`)
console.log(`C vaut "428""12" (vérification en console: C = ${C}`)
/***********************************************************************
************************************************************************
************************************************************************/
/* let A = 5
let B = 7
let X = A // X=5
let Y = B // Y=7
X = Y // X=7
Y = X
console.log(X, Y) */