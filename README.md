# Exercices d'algo

## Partage cours d'algorythmie

* Partage & sauvegarde **exercices** cours d'``algorythmie`` de (2020-2021), *(cours de prépa. au **TP RNCP 6** de ``CDA`` *(Concepteur développeur d'application, fullstack **Java**)*, école **HumanBooster**, Villeurbanne (69))*.
  
* Exercices réalisés en ``JavaScript`` & en ``Java``, *Exo. dispo. en **PSEUDO CODE** (voir source)*
  
* **source**: *- cours **CDA** 2020-2021 **HB***
              *- énoncés & corrigé en PSEUDO CODE [ICI](http://cours.pise.info/algo/enonces1.htm#En1.1)

### LES VARIABLES

**EXO 1.1**

+ Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ?

```js
// Variables A, B en Entier
let A = 1
let B = A + 3
A = 3   // A vaut 3 // B vaut 4

// vérification en console
console.log(`A vaut 3 (vérification en console: A = ${A}`)
console.log(`B vaut 4 (vérification en console: B = ${B}`) 
```

**EXO 1.2**

+ Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ?

```js
// Variables A, B, C en Entier
let A = 5
let B = 3
let C = A + B // C=5+3
A = 2         // A change de valeur & passe de 5 à 2
C = B - A     // C=3-2
// donc A = 2 & B = 3 

console.log(`A vaut 2 (vérification en console: A = ${A}`)
console.log(`B vaut 3 (vérification en console: B = ${B}`)
console.log(`C vaut ... (vérification en console: C = ${C}`)
// OK, FIN
```

**EXO 1.3**

+ Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ?

```js
// Variables A, B en Entier
let A = 5
let B = A + 4   // B = 9
A = A + 1       // A = 6
B = A - 4       // B = 6 - 4 // B = 2

console.log(`A vaut 6 (vérification en console: A = ${A}`)
console.log(`B vaut 2 (vérification en console: B = ${B}`)
```

**EXO 1.4**

+ Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ?

```js
// Variables A, B, C en Entier
let A = 3
let B = 10
let C = A + B   // C = 13
B = A + B       // B = 13
A = C           // A = 13 (C = 13) 

console.log(`A vaut 13 (vérification en console: A = ${A}`)
console.log(`B vaut 13 (vérification en console: B = ${B}`)
console.log(`C vaut 13 (vérification en console: C = ${C}`)
// OK FIN ALGO
```
**1.5**

+ Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ? les deux dernières instructions permettent-elles d’échanger les deux valeurs de B et A ?
  
```js
// Variables A, B en Entier
let A = 5
let B = 2
A = B       // A = 2 
B = A       // B = 2

console.log(`A vaut 2 (vérification en console: A = ${A}`)
console.log(`B vaut 2 (vérification en console: B = ${B}`)
```
**EXO 1.6**

+ ENONCE: *Plus difficile, mais c’est un classique absolu, qu’il faut absolument maîtriser : écrire un algorithme permettant d’échanger les valeurs de deux variables A et B, et ce quel que soit leur contenu préalable.*

```js
let A = "foo"
let B = "bar"
let C = A   // utilisons 1 variable temporaire    
A = B       // A = B (soit "bar")
B = C       // B = A (soit "foo")
C = A + B   // en JS, les chaînes de charactère(string) se conctaîne (C = "barfoo")

console.log(`A vaut "bar" (vérification en console: A = ${A}`)
console.log(`B vaut "foo" (vérification en console: B = ${B}`)
console.log(`C vaut "barfoo" (vérification en console: C = ${C}`)
```

**EXO 1.8**

+ Que produit l’algorithme suivant ?

```js
// Variables A, B, C en Caractères
let A = "428"
let B = "12"
let C = A + B

console.log(`A vaut "428" (vérification en console: A = ${A}`)
console.log(`B vaut "12" (vérification en console: B = ${B}`)
console.log(`C vaut "42812" (vérification en console: C = ${C}`)
```
**1.9 EXO**

+ Que produit l’algorithme suivant ?

```js
// Variables A, B, C en Caractères
let A = "423"
let B = "12"
let C = A && B    // C = "423""12" (en JS, on ne peut pas concaténer avec ET)
// mais en JS, l'addition de 2 string fonctionne et les concatène
// ! ceci est propre au JS (à vérifier ds autres langages)

console.log(`A vaut "423" (vérification en console: A = ${A}`)
console.log(`B vaut "12" (vérification en console: B = ${B}`)
console.log(`C vaut "428""12" (vérification en console: C = ${C}`)```